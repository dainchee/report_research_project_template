"""
Start ipython with the command `ipython --matplotlib`

Then, run this script with:

```
run job_sim_predaprey.py
```
"""
from pathlib import Path

import matplotlib.pyplot as plt

from fluidsim.solvers.models0d.predaprey.solver import Simul

here = Path(__file__).absolute().parent

params = Simul.create_default_params()

params.time_stepping.deltat0 = 0.1
params.time_stepping.t_end = 20

params.output.periods_print.print_stdout = 0.01

sim = Simul(params)

sim.state.state_phys.set_var("X", sim.Xs)
sim.state.state_phys.set_var("Y", sim.Ys)

# sim.output.phys_fields.plot()
sim.time_stepping.start()

sim.output.print_stdout.plot_XY()

# Note: if you want to modify the figure and/or save it, you can use
ax = plt.gca()
fig = ax.figure

ax.set_title("Evolution of the Prey and Predator from a stable point")

ax.set_xlim(0,5)
ax.set_ylim(0,5)
fig_dir = here.parent / "fig"
fig.savefig(fig_dir / "PreyPredXYStable.png")


sim.output.print_stdout.plot_XY_vs_time()

ax = plt.gca()
fig = ax.figure

ax.set_title("Evolution of the Prey and Predator as a function of time starting from a stable point")

fig_dir = here.parent / "fig"
fig.savefig(fig_dir / "PreyPredXYtStable.png")

plt.show()
